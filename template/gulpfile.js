'use strict';

var argv = require('yargs').argv;
var gulp = require('gulp');
var gutil = require('gulp-util');
var bump = require('gulp-bump');
var git = require('gulp-git');
var del = require('del');
var fs = require('fs');


var paths = {
  src: {
    js: 'lib/**/*.js'
  },

  versions: [
    './package.json'
  ]
};

var config = {
};


function getPackageJsonVersion () {
  return JSON.parse(fs.readFileSync('./package.json', 'utf8')).version;
};


gulp.task('clean', () => {});

gulp.task('bump-version', () => {
  return gulp.src(paths.versions)
    .pipe(bump({type: typeof(argv['bump-type']) === 'undefined' ? 'patch' : argv['bump-type']}).on('error', gutil.log))
    .pipe(gulp.dest('./'));
});

gulp.task('commit-changes', () => {
  let version = getPackageJsonVersion();
  return gulp.src('.')
    .pipe(git.add())
    .pipe(git.commit(`[Prerelease] Bumped version number to ${version}`));
});

gulp.task('create-new-tag', done => {
  let version = getPackageJsonVersion();
  git.tag(version, 'Created Tag for version: ' + version, function (error) {
    if (error) {
      return done(error);
    }
    git.push('origin', 'master', {args: '--tags'}, done);
  });
});

gulp.task('test', done => {
});

gulp.task('test.watch', done => {
});

gulp.task('release', gulp.series('bump-version', 'commit-changes', 'create-new-tag'));

gulp.task('default', gulp.task('release'));
