# nmicro template repository

this repo is a template for creating raml + hapi-based nodejs services

## usage

## references:

https://github.com/dave-irvine/node-hapi-raml

https://www.npmjs.com/package/slush-hapi

https://github.com/raml-org/raml-spec/blob/raml-10/versions/raml-10/raml-10.md/

https://github.com/dave-irvine/node-hapi-raml

http://hapijs.com/
